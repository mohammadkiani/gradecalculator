package com.ut.gradecalculator2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class GradeActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String TAG = "GradeCalculator";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade);

        Button minBtn = findViewById(R.id.minBtn);
        minBtn.setOnClickListener(this);
        Button maxBtn = findViewById(R.id.maxBtn);
        maxBtn.setOnClickListener(this);
        Button avgBtn = findViewById(R.id.avgBtn);
        avgBtn.setOnClickListener(this);
    }

    private void generateReport(int id) {
        String grade;
        String tag;
        switch (id) {
            case R.id.minBtn:
                grade = getMin();
                tag = "MIN";
                break;
            case R.id.maxBtn:
                grade = getMax();
                tag = "MAX";
                break;
            case R.id.avgBtn:
                grade = getAverage();
                tag = "AVG";
                break;
            default:
                grade = "0";
                tag = "";
                break;
        }

        Intent intent = new Intent(this, ReportActivity.class);
        intent.putExtra("ios", getGrade(R.id.iosField));
        intent.putExtra("android", getGrade(R.id.androidField));
        intent.putExtra("swift", getGrade(R.id.swiftField));
        intent.putExtra("java", getGrade(R.id.javaField));
        intent.putExtra("grade", grade);
        intent.putExtra(TAG, tag);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        generateReport(v.getId());
    }

    private String getAverage() {
        Double iosMark = Double.valueOf(getGrade(R.id.iosField));
        Double androidMark = Double.valueOf(getGrade(R.id.androidField));
        Double swiftMark = Double.valueOf(getGrade(R.id.swiftField));
        Double javaMark = Double.valueOf(getGrade(R.id.javaField));
        Double[] grades = {iosMark, androidMark, swiftMark, javaMark};
        Double sum = 0.0;
        for (Double grade : grades)
            sum += grade;
        Double avg = sum / grades.length;
        return avg.toString();
    }

    private String getMax() {
        Double iosMark = Double.valueOf(getGrade(R.id.iosField));
        Double androidMark = Double.valueOf(getGrade(R.id.androidField));
        Double swiftMark = Double.valueOf(getGrade(R.id.swiftField));
        Double javaMark = Double.valueOf(getGrade(R.id.javaField));
        Double[] grades = {iosMark, androidMark, swiftMark, javaMark};
        Double max = grades[0];
        for (Double grade : grades)
            max = Math.max(max, grade);
        return max.toString();
    }

    private String getMin() {
        Double iosMark = Double.valueOf(getGrade(R.id.iosField));
        Double androidMark = Double.valueOf(getGrade(R.id.androidField));
        Double swiftMark = Double.valueOf(getGrade(R.id.swiftField));
        Double javaMark = Double.valueOf(getGrade(R.id.javaField));
        Double[] grades = {iosMark, androidMark, swiftMark, javaMark};
        Double min = grades[0];
        for (Double grade : grades)
            min = Math.min(min, grade);
        return min.toString();
    }

    private String getGrade(int id) {
        EditText grade = findViewById(id);
        if (!TextUtils.isEmpty(grade.getText().toString())) {
            return grade.getText().toString();
        }
        return "0";
    }
}
